const express = require('express');
const pelicula = require('../controllers/router_peliculas');
const auth = require('../middelwares/auth')
const userCtrl = require('../controllers/user')

const app = express.Router();

//listar pelicula app
app.get('/listPeliculas', auth,pelicula.listarPeliculas);

//listar peliculas web
app.get('/listarContenido', pelicula.listarContenido);

//agregar pelicula post
app.post('/addPelicula', pelicula.addPelicula);

//editar pelicula
    //get - pedir datos a editar
     app.get('/editPelicula', pelicula.editPelicula);
    //post  - guadar datos a editar
     app.post('/edit_Pelicula', pelicula.edit_Pelicula);


//eliminar pelicula
app.get('/deletPelicula', pelicula.deletPelicula);

app.get('/private', auth, (req, res) => {
    res.status(200).send({ message: 'Tienes acceso' })
  })

 app.post('/signup', userCtrl.signUp)
app.post('/signin', userCtrl.signIn)
  
module.exports = app;
