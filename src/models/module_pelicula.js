const { Schema, model } =require('mongoose');

const pelicula = Schema({
    titulo:    {type: String, require:[true, "titulo abligatorio"]},
    imagen:    {type: String, require: [true, "imagen requerida"]},
    categoria:    {type: String, require: [true, "categoria requerida"]},
    descripcion:    {type: String, require: [true, "descripcion requerida"]},
    url:    {type: String, require: [true, "url requerida"]},
});

module.exports = model('peliculas', pelicula);