//const express = require('express');


//modelo db
const pelicula = require('../models/module_pelicula');


//coneccion a mongodb 
const pool = require('../setting/db');


//app
let listarPeliculas = async (req, res) => {
    const model = await pelicula.find()
    pelicula.countDocuments({}, (err, total) => {
     if(err){
         return res.json({
             status: 400,
             mensaje: "Error al leer archivo",
             err
         })
     }
      res.json({status: 200, total,model }) 
      console.log(module);
      
    })
};

//web
let listarContenido = async (req, res) => {
    const peliculas = await pelicula.find()
    pelicula.countDocuments({}, (err, total) =>{
        if(err){
            return res.json({
                stuatus: 400,
                mensaje: "Error al leer el archivo",
                err
            })
        }
        res.render('index', { peliculas });
    })
}
//agregar pelicula
let addPelicula = async (req, res) =>{
    const { titulo, imagen, categoria,descripcion, url } =req.body;
    const peliculas = await new pelicula({
        titulo,
        imagen,
        categoria,
        descripcion,
        url
    })
    peliculas.save((err, data) => {
        if(err){
            return res.json({
                status: 200,
                mensaje: "error al agregar la pelicula",
                err
            
            })
        }
        })
    res.redirect('/api/listarContenido');
};

//editar pelicula get
let editPelicula = async(req, res) => {
    let id = req.params._id;
    const peliculas = await pelicula.findById(id);
    res.render('editPelicula', {peliculas})
};

// editar pelicula post
let edit_Pelicula = async(req, res) => {
    let id = req.params;
    const {titulo, imagen, categoria, descripcion,url } = req.body;
    const edit_pelicula ={
        titulo,
        imagen,
        categoria,
        descripcion,
        url
    }
      const peliculas = pelicula.update({id: id}, edit_pelicula);
      res.redirect('/api/listarContenido');
};


//eliminar pelicula get
let deletPelicula = async (req, res) =>{
    let id = req.params;
    await pelicula.findOneAndDelete({_id: id});
    res.redirect('/api/listarContenido');
}


    module.exports = {
        listarPeliculas,
        listarContenido,
        addPelicula,
        editPelicula,
        edit_Pelicula,
        deletPelicula
    }
