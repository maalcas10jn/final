const express = require('express');
const morgan  = require('morgan');
const bodyparser = require('body-parser');
const path=require('path');
const ejs = require('ejs');
const jwt =require('jsonwebtoken');

const app = express();
 


app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'))
// ejs
//app.set('view engine', '.ejs');




//middeleware
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json({extended: true}));

//router
app.use('/api', require('./routers/app'));

//public
app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'),() => {
    console.log('server on port: ', app.get('port'));

});